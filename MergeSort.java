/* INF1010 - Oblig 6 v14
 * Stian Kongsvik
 * stiako
 */

public class MergeSort extends Thread{
    private String[] items1, items2, mergedItems;
    private Monitor monitor;
    private int mergedLength, length1, length2;
    private int count1 = 0;
    private int count2 = 0;
    private int check = 0;

    MergeSort(String[] items1, String[] items2, Monitor monitor){
        this.items1 = items1;
        this.items2 = items2;
        length1 = items1.length;
        length2 = items2.length;
        mergedLength = length1 + length2;
        mergedItems = new String[mergedLength];
        this.monitor = monitor;
    }

    public void run(){
        // Merges two sorted arrays into a new called "mergedItems".
        // The arrays can have different lengths, and as soon as one
        // has been iterated through, the rest of the other will be copied
        // directly to the final array.
        while(check < mergedLength){
            if((count1 < length1) && (count2 < length2)){
                if(items1[count1].compareTo(items2[count2]) < 0){
                    mergedItems[check++] = items1[count1++];
                } else {
                    mergedItems[check++] = items2[count2++];
                }
            } else if(count2 >= length2){
                mergedItems[check++] = items1[count1++];
            } else if(count1 >= length1){
                mergedItems[check++] = items2[count2++];
            }
        }
        // Sends the result of this merge to the monitor
        monitor.setArray(mergedItems);
    }
}
