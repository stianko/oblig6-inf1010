/* INF1010 - Oblig 6 v14
 * Stian Kongsvik
 * stiako
 */

import java.io.*;

class ResultWriter{
    private String filename;
    private PrintWriter out;

    ResultWriter(String filename){
        this.filename = filename;
    }
    public void writeFile(String[] items){
        try{
            out = new PrintWriter(filename);
            out.println(items.length);
            for(int i = 0; i < items.length; i++){
                out.println(items[i]);
            }
            out.close();
        } catch(IOException e){
            System.out.println("Error writing file.");
        }
    }
}
