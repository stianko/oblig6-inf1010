/* INF1010 - Oblig 6 v14
 * Stian Kongsvik
 * stiako
 */

import java.util.*;
import java.io.*;

public class ParseFile{
    private int wordCnt;
    private String[] items;
    private Scanner sc;
    private Monitor monitor;
    private int threadCnt;
    public String outfile;

    ParseFile(int threadCnt, String filename, String outfile){
        // MergeSort-threads is made by the monitor, so dividing by 2 gives threadCnt-1 total threads
        this.threadCnt = threadCnt/ 2;
        this.outfile = outfile;
        parser(filename);
    }

    private void parser(String f){
        try{
            sc = new Scanner(new File(f));
            wordCnt = Integer.parseInt(sc.nextLine());
            items = new String[wordCnt];
            for(int i = 0; i < wordCnt; i++){
                items[i] = sc.nextLine();
            }
            if(sc.hasNext()){
                System.out.println("More words than index number in file");
                System.exit(0);
            }
        } catch (IOException e){
            System.out.println("Reading file failed.");
            System.exit(0);
        } catch (NoSuchElementException nsee){
            System.out.println("Too few words compared to index number in file");
            System.exit(0);
        } catch (NumberFormatException nfe){
            System.out.println("First line not a number");
            System.exit(0);
        }
        // Makes a monitor that's passed along to all threads
        monitor = new Monitor(wordCnt, outfile);
        int threadSize = wordCnt / threadCnt;

        // Makes threads
        for(int i = 0; i < (threadCnt - 1); i++){
            sortMethod(i * threadSize, (i * threadSize) + threadSize);
        }
        sortMethod((threadCnt - 1) * threadSize, wordCnt);
    }
    //Could be changed to call other sort methods
    public void sortMethod(int start, int end){
        new BubbleSort(items, start, end, monitor).start();
    }
}
