/* INF1010 - Oblig 6 v14
 * Stian Kongsvik
 * stiako
 */

public class Monitor{
    private String[] storedSortedArray;
    private final int wordCnt;
    private long startTime;
    private double time;
    private ResultWriter rw;
    private int numOfThreads = 0;

    Monitor(int wordCnt, String outfile){
        this.wordCnt = wordCnt;
        rw = new ResultWriter(outfile);
        startTime = System.nanoTime();
    }

    // This method is called at the end of all sorting threads,
    // and is synchronized so that all other threads are queued
    // until the current is finished.
    // It will alternate between storing an array, and merging
    // the previously stored array with the current.
    // When it gets passed an array with the same length as the
    // original unsorted array, it finishes and calls time and
    // write methods.
    public synchronized void setArray(String[] sortedArray){
        threadCounter();
        if(sortedArray.length == wordCnt){
            if(sortedArray[wordCnt - 1] == null){
                System.out.println("CRITCAL ERROR: Last item in final sorted array was null");
                System.exit(0);
            }
            time = (System.nanoTime() - startTime)/(double) 1000000;
            System.out.println(timer() + ", number of threads: " + numOfThreads );
            rw.writeFile(sortedArray);
        }
        if(storedSortedArray == null){
            storedSortedArray = sortedArray;
        } else {
            new MergeSort(storedSortedArray, sortedArray, this).start();
            storedSortedArray = null;
        }
    }

    public String timer(){
        if(time >= 1000){
            return "" + time/1000 + " s";
        }
        return "" + time + " ms";
    }

    public void threadCounter(){
        numOfThreads++;
    }
}
