/* INF1010 - Oblig 6 v14
 * Stian Kongsvik
 * stiako
 */

public class BubbleSort extends Thread{
    private String[] items;
    private String[] sortedItems;
    private int start, end;
    private boolean swap = true;
    private Monitor monitor;

    BubbleSort(String[] items, int start, int end, Monitor monitor){
        this.items = items;
        this.start = start;
        this.end = end;
        sortedItems = new String[end - start];
        this.monitor = monitor;
    }

    public void run(){
        String tmp;
        // Copies the part of the original array that this thread is going to sort.
        for(int i = 0; i < (end - start); i++){
            sortedItems[i] = items[i + start];
        }
        // Bubble sort compares each item of the array to the next, and swaps them
        // if the next is bigger than the current. Repeats this until no items are
        // swapped.
        // The outer loop invariant is "swap == true"
        // The inner for-loop invariant is "i < (end - start - 1)
        while(swap){
            swap = false;
            for(int i = 0; i < (end - start - 1); i++){
                if(sortedItems[i].compareTo(sortedItems[i + 1]) > 0){
                    tmp = sortedItems[i];
                    sortedItems[i] = sortedItems[i + 1];
                    sortedItems[i + 1] = tmp;
                    swap = true;
                }
            }
        }
        monitor.setArray(sortedItems);
    }
}

